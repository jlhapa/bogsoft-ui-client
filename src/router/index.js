import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Company from '../views/Company.vue'
import Department from '../views/Department.vue'
import Employee from '../views/Employee.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/company',
    name: 'Company',
    component: Company
  },
  {
    path: '/department',
    name: 'Department',
    component: Department
  },
  {
    path: '/Employee',
    name: 'Employee',
    component: Employee
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
